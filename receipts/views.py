from django.shortcuts import redirect, render
from receipts.models import Receipt

def home(request):
    receipts = Receipt.objects.all()
    context = {'receipts': receipts}
    return render(request, 'receipts/home.html', context)
def redirect_to_receipts(request):
    return redirect('/receipts/')


