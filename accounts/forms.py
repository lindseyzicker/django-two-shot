from django import forms

class login(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
)




# Create a form that has two
# fields in the accounts Django app with:
# A username field with a maximum length
# of 150 characters
# A password field with:
# a maximum length of 150 characters
# the PasswordInput as its widget
