from django.urls import path, include
from accounts.forms import login

urlpatterns = [
    path('login/', login, name='login'),
    path('accounts/', include, 'accounts.urls'),
]
